var threejs = (function(app){
  app.utils = {
    degToRad: function(degree){
      return degree*(Math.PI/180);
    }
  };

  return app;
}(threejs || {}));
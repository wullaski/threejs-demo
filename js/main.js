var threejs = (function(app){

  var SCREEN_WIDTH = window.innerWidth;
  var SCREEN_HEIGHT = window.innerHeight;
  var container, stats;
  var camera, scene, renderer;
  var clock = new THREE.Clock();


  init();
  animate();

  function init() {
    container = document.createElement( 'div' );
    document.body.appendChild( container );
    container.setAttribute("id", "container");
    
    // CAMERA
    camera = new THREE.PerspectiveCamera( 40, SCREEN_WIDTH / SCREEN_HEIGHT, 1, 10000 );
    camera.position.set( 100, 100, 200 );
    
    // SCENE
    scene = new THREE.Scene();
    
    // CONTROLS
    controls = new THREE.OrbitControls( camera, container );
    controls.maxPolarAngle = 0.9 * Math.PI / 2;
    controls.enableZoom = true;
    
    // LIGHTS
    var keyLight = new THREE.PointLight( 0xffffff, 0.4 );
    keyLight.position.x = 0;
    keyLight.position.y = 40;
    keyLight.position.z = 200;
    scene.add( keyLight );

    var fillLight = new THREE.PointLight( 0xffffff, 0.3 );
    fillLight.position.x = 300;
    fillLight.position.y = 250;
    fillLight.position.z = 500;
    scene.add( fillLight );

    var Backlight = new THREE.PointLight( 0xffffff, 0.3 );
    Backlight.position.x = 300;
    Backlight.position.y = 250;
    Backlight.position.z = -500;
    scene.add( Backlight );
    
    // SKYDOME
    var vertexShader = document.getElementById( 'vertexShader' ).textContent;
    var fragmentShader = document.getElementById( 'fragmentShader' ).textContent;
    var uniforms = {
      topColor:    { type: "c", value: new THREE.Color( 0x0077ff ) },
      bottomColor: { type: "c", value: new THREE.Color( 0xffffff ) },
      offset:    { type: "f", value: 400 },
      exponent:  { type: "f", value: 0.6 }
    };
    uniforms.topColor.value.copy( keyLight.color );
    var skyGeo = new THREE.SphereGeometry( 4000, 32, 15 );
    var skyMat = new THREE.ShaderMaterial( {
      uniforms: uniforms,
      vertexShader: vertexShader,
      fragmentShader: fragmentShader,
      side: THREE.BackSide
    } );
    var sky = new THREE.Mesh( skyGeo, skyMat );
    scene.add( sky );
    
    // RENDERER
    renderer = new THREE.WebGLRenderer( { antialias: true } );
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( SCREEN_WIDTH, SCREEN_HEIGHT );
    container.appendChild( renderer.domElement );
    renderer.gammaInput = true;
    renderer.gammaOutput = true;
    
    // STATS
    stats = new Stats();
    container.appendChild( stats.dom );
    
    // MODEL
    // var loader = new THREE.JSONLoader();
    // loader.load( "obj/lightmap/lightmap.js", function ( geometry, materials ) {
    //   for ( var i = 0; i < materials.length; i ++ ) {
    //     materials[ i ].lightMapIntensity = 0.75;
    //   }
    //   var mesh = new THREE.Mesh( geometry, new THREE.MultiMaterial( materials ) );
    //   mesh.scale.multiplyScalar( 100 );
    //   scene.add( mesh );
    // } );

    //OBJ Loader

    var onProgress = function ( xhr ) {
      if ( xhr.lengthComputable ) {
        var percentComplete = xhr.loaded / xhr.total * 100;
        console.log( Math.round(percentComplete, 2) + '% downloaded' );
      }
    };
    var onError = function ( xhr ) { };


    THREE.Loader.Handlers.add( /\.dds$/i, new THREE.DDSLoader() );
    var mtlLoader = new THREE.MTLLoader();
    mtlLoader.setBaseUrl( 'obj/cornerthing/' );
    mtlLoader.setPath( 'obj/cornerthing/' );
    mtlLoader.load( 'cornerthing.mtl', function( materials ) {
      materials.preload();
      var objLoader = new THREE.OBJLoader();
      objLoader.setMaterials( materials );
      objLoader.setPath( 'obj/cornerthing/' );
      objLoader.load( 'cornerthing.obj', function ( object ) {
        
        buildPanel(object);
      }, onProgress, onError );
    });



    var buildPanel = function(object){
      

      var degreeToRadian = function(degree)   { return degree*(Math.PI/180); }
      var cloneX = 0,
          cloneY = 0,
          cloneZ = 0;
        console.log(app.utils)

        var cornerBL = object.clone();
        cornerBL.rotation.x = app.utils.degToRad(-90);

        var cornerTL = object.clone();
        cornerTL.rotation.x = app.utils.degToRad(180);
        // cornerTL.rotation.z = app.utils.degToRad(180);

        cornerTL.position.y = 40; // or any other coordinates

        var cornerBR = object.clone();
        cornerBR.rotation.y = app.utils.degToRad(-180);
        cornerBR.position.x = 80;
      

        var cornerTR = object.clone();
        cornerTR.rotation.y = app.utils.degToRad(-180);
        cornerTR.rotation.x = app.utils.degToRad(-90);
        cornerTR.position.x = 80;
        cornerTR.position.y = 40;
        
        
        scene.add(cornerBL);
        scene.add(cornerTL);
        scene.add(cornerBR);
        scene.add(cornerTR);


        // panel

        var geometry = new THREE.BoxGeometry( 80, 40, 1 );
        var material = new THREE.MeshStandardMaterial({color: 0xcccccc, roughness: .5, metalness: .4});
        var cube = new THREE.Mesh( geometry, material );
        cube.position.x = 40;
        cube.position.y = 20;
        cube.position.z = -1;
        scene.add( cube );


        var updatePanel = function(){
          var slider = document.getElementById("slider");
          var sliderValue = Number(slider.value);
          cube.scale.x = (sliderValue/80);
          cube.position.x = 40 * (sliderValue/80);
          
          cornerTR.position.x = sliderValue;
          cornerBR.position.x = sliderValue;
        }
        window.addEventListener('change', updatePanel, false);
    }
    
    //
    window.addEventListener( 'resize', onWindowResize, false );
  }
  function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
  }
  //
  function animate() {
    requestAnimationFrame( animate );
    renderer.render( scene, camera );
    stats.update();
  }

  return app;

}(threejs || {}));